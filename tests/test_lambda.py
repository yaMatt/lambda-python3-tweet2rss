import unittest
import json

from tweepy.error import TweepError

import lambda_function


class TestHandler(unittest.TestCase):
    def setUp(self):
        self.apigateway_request_event_dict = json.load(
            open("./tests/res/apig.request.sample.json")
        )

    def test_handler(self):
        response = lambda_function.lambda_handler(
            self.apigateway_request_event_dict, None
        )
        self.assertEqual(response["statusCode"], 200)
        self.assertEqual(response["headers"]["Content-Type"], "application/atom+xml")
        self.assertTrue("body" in response)
        print(response["body"])

class TestGetUser(unittest.TestCase):
    def test_existing_username(self):
        lambda_function.get_user("twitter")

    def test_not_existing_username(self):
        with self.assertRaises(TweepError):
            lambda_function.get_user("wieyae3L")
